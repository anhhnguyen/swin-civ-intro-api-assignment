﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace APIHandler
{
    class Program
    {
        static void Main(string[] args)
        {
            TourSystemHandler th = new TourSystemHandler();
            Customer[] customers = th.GetCustomers();
            Customer customer = th.GetCustomer(521);
            Tour[] tours = th.GetTours();
//            Tour tour = th.GetTour("GR1");
            Booking booking = th.GetBooking(521);
            
        }
    }

    public class TourSystemHandler {
        public string BaseUrl { get; set; }
        public string CustEP { get; set; }
        public string TourEP { get; set; }
        public string BookingEP { get; set; }

        public string Id { get; set; }

        public APIHandler aph { get; set; }

        public TourSystemHandler() {
            BaseUrl = "http://swin-sem1-api.azurewebsites.net/index.php/";
            CustEP = "customers";
            TourEP = "tours";
            BookingEP = "bookings";

            aph = new APIHandler();
        }

        public Customer[] GetCustomers() {
            Task<string> t = aph.SendRequest(BaseUrl + CustEP);
            //while(!t.IsCompleted) { }
            string ts = t.Result;

            return JsonConvert.DeserializeObject<List<Customer>>(ts).ToArray();
        }

        public Customer GetCustomer(int id) {
            Task<string> t = aph.SendRequest(BaseUrl + CustEP.Remove(CustEP.Length - 1) + "/" + id.ToString());
            //while(!t.IsCompleted) { }
            string ts = t.Result;
            return JsonConvert.DeserializeObject<List<Customer>>(ts).ToArray()[0];
        }

        public Tour[] GetTours() {
            Task<string> t = aph.SendRequest(BaseUrl + TourEP);
            //while(!t.IsCompleted) { }
            string ts = t.Result;
            return JsonConvert.DeserializeObject<List<Tour>>(ts).ToArray();
        }

        public Tour GetTour(string ac) {
            Task<string> t = aph.SendRequest(BaseUrl + TourEP.Remove(TourEP.Length - 1) + "/" + ac);
            //while(!t.IsCompleted) { }
            string ts = t.Result;
            return JsonConvert.DeserializeObject<List<Tour>>(ts).ToArray()[0];
        }

        public Booking GetBooking(int id) {
            Task<string> t = aph.SendRequest(BaseUrl + BookingEP + "/" + id.ToString());
            //while(!t.IsCompleted) { }
            string ts = t.Result;
            return JsonConvert.DeserializeObject<List<Booking>>(ts).ToArray()[0];
        }
    }
    public class APIHandler {
        public HttpClient HttpClient { get; set; }

        public APIHandler() {
            HttpClient = new HttpClient();
        }

        public async Task<string> SendRequest(string uri) {
            Task<HttpResponseMessage> resp = HttpClient.GetAsync(uri);

            return await resp.Result.Content.ReadAsStringAsync();
        }
    }

    public class Customer {
        public int CustomerCode { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PostCode { get; set; }
        public string Phone { get; set; }
    }

    public class Tour {
        public string Adventure_Code { get; set; }
        public string Start_Date { get; set; }
        public string Desc { get; set; }
        public int Duration { get; set; }
        public int Price { get; set; }  
    }

    public class Booking {
        public int CustomerCode { get; set; }
        public string Name { get; set; }     
        public string Adventure_Code { get; set; }
        public string Description { get; set; }
        public string Start_Date { get; set; }
        public int Duration { get; set; }
    }
}
